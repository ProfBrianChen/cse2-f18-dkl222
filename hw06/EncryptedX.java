import java.util.Scanner;
public class EncryptedX {

	public static void main(String[] args) {
		boolean correctInput;
		int userInput;
		Scanner scnr = new Scanner (System.in);
		
		//Validating as an integer
		System.out.println("Input an integer between 0 and 100:");
		correctInput = scnr.hasNextInt();
		
		while (correctInput == false) {
				System.out.println("Input not an integer. Please input an integer between 0 and 100:");
				correctInput = scnr.hasNextInt();
		}
		
		userInput = scnr.nextInt();
		boolean correctNumberInput;
		
		if (userInput < 0 && userInput > 100) {
			correctNumberInput = false;
		}
		else {
			correctNumberInput = true;
		}
		
		while (correctNumberInput == false) {
			System.out.println("Please input an integer between 0 and 100:");
			userInput = scnr.nextInt();
			
			if (userInput < 0 && userInput > 100) {
				correctNumberInput = false;
			}
			else {
				correctNumberInput = true;
			}
		}
		
		//Printing output
		for (int i = 0; i < userInput; i++) {
			for (int j = 0; j < userInput; j++) {
				if (i == j || i + j == userInput - 1) {
					System.out.print(" ");
				}
				else {
					System.out.print("*");
				}
			}
			System.out.println("");
		}
		

	}

}