public class PassingArrays {

	public static void main(String[] args) {
		int[] array0 = {1, 2, 3, 4, 5, 6, 7, 8};
		int[] array1 = copy(array0);
		int[] array2 = copy(array0);
		
		//Invert and print array0
		inverter(array0);
		print(array0);
		
		//Invert2 and print array1
		inverter2(array1);
		print(array1);
		
		//Create array3 from array2 and print
		int[] array3 = inverter2(array2);
		print(array3);
	}
	
	//Copies given array into new array
	public static int[] copy(int[] givenArray) {
		int[] newArray = new int[givenArray.length];
		
		for(int i = 0; i < givenArray.length; i++) {
			newArray[i] = givenArray[i];
		}
		
		return newArray;
	}
	
	//Inverts given array
	public static int[] inverter (int[] givenArray) {
		for (int i = 0; i < givenArray.length / 2; i++) {
			int temp = givenArray[i];
			givenArray[i] = givenArray[givenArray.length - i - 1];
			givenArray[givenArray.length - i - 1] = temp;
		}
		return givenArray;
	}
	
	//Inverts the copy of the given array
	public static int[] inverter2 (int[]givenArray) {
		int[] givenArray2 = copy(givenArray);
		inverter(givenArray2);
		
		return givenArray2;
	}
	
	//Prints all elements of the array
	public static void print(int[] givenArray) {
		for(int i = 0; i < givenArray.length; i++) {
			System.out.print(givenArray[i] + " ");
		}
		System.out.println("");
	}

}