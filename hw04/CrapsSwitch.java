//David K. Lee
//9.25.2018
//CrapsSwitch.java
///////////////////
import java.util.Scanner;

public class CrapsSwitch{
  public static void main(String args[]){
    Scanner askScanner = new Scanner(System.in);
    int answer;
    
    //Asking prompt
    System.out.println("Random dice or choose dice number?");
    System.out.println("\"0\" for RANDOM");
    System.out.println("\"1\" for CHOOSE:");
    answer = askScanner.nextInt();
    
    int dice1 = 0;
    int dice2 = 0;
    int total = dice1 + dice2;
    
    switch (answer){
      case 0:{
        dice1 = (int)((Math.random() * 5) + 1);
        dice2 = (int)((Math.random() * 5) + 1);
        total = dice1 + dice2;
        break;
      }
      case 1:{
        System.out.println("Input dice 1 (1-6): ");
        dice1 = askScanner.nextInt();
        System.out.println("Input dice 2 (1-6): ");
        dice2 = askScanner.nextInt();
        total = dice1 + dice2;
        break;
      }
    }
    
    switch (total){ 
      case 2:{     
        System.out.println("Dice 1: " + dice1);
        System.out.println("Dice 2: " + dice2);        
        System.out.println("Dice Total: " + total);        
        System.out.println("Roll Type: Snake Eyes");        
        break;        
      }      
      case 3:{      
        System.out.println("Dice 1: " + dice1);        
        System.out.println("Dice 2: " + dice2);              
        System.out.println("Dice Total: " + total);      
        System.out.println("Roll Type: Ace Deuce");     
        break;    
      }    
      case 4:{        
        switch (dice1){ 
          case 2:{          
            System.out.println("Dice 1: " + dice1);            
            System.out.println("Dice 2: " + dice2);            
            System.out.println("Dice Total: " + total);
            System.out.println("Roll Type: Hard Four");
            break;
          }
          default:{          
            System.out.println("Dice 1: " + dice1);            
            System.out.println("Dice 2: " + dice2);            
            System.out.println("Dice Total: " + total);            
            System.out.println("Roll Type: Easy Four");            
            break;            
          }          
        }        
      }      
      case 5:{      
        System.out.println("Dice 1: " + dice1);                
        System.out.println("Dice 2: " + dice2);        
        System.out.println("Dice Total: " + total);        
        System.out.println("Roll Type: Fever Five");        
        break;        
      }      
      case 6:{            
        switch (dice1){        
          case 3:{          
            System.out.println("Dice 1: " + dice1);           
            System.out.println("Dice 2: " + dice2);            
            System.out.println("Dice Total: " + total);            
            System.out.println("Roll Type: Hard Six");            
            break;            
          }          
          default:{          
            System.out.println("Dice 1: " + dice1);            
            System.out.println("Dice 2: " + dice2);            
            System.out.println("Dice Total: " + total);          
            System.out.println("Roll Type: Easy Six");
            break;
          }
        }
      }      
      case 7:{
        System.out.println("Dice 1: " + dice1);                  
        System.out.println("Dice 2: " + dice2);       
        System.out.println("Dice Total: " + total);
        System.out.println("Roll Type: Seven out");
        break;
      }
      case 8:{
        switch (dice1){
          case 4:{
            System.out.println("Dice 1: " + dice1);            
            System.out.println("Dice 2: " + dice2);            
            System.out.println("Dice Total: " + total);            
            System.out.println("Roll Type: Hard Eight");            
            break;            
          }          
          default:{                                
            System.out.println("Dice 1: " + dice1);        
            System.out.println("Dice 2: " + dice2);       
            System.out.println("Dice Total: " + total);           
            System.out.println("Roll Type: Easy Eight");         
            break;           
          }       
        }       
      }    
      case 9:{    
        System.out.println("Dice 1: " + dice1);     
        System.out.println("Dice 2: " + dice2);     
        System.out.println("Dice Total: " + total);      
        System.out.println("Roll Type: Nine");     
        break;      
      }    
      case 10:{   
        switch (dice1){     
          case 5:{   
            System.out.println("Dice 1: " + dice1);         
            System.out.println("Dice 2: " + dice2);         
            System.out.println("Dice Total: " + total);         
            System.out.println("Roll Type: Hard Ten");         
            break;          
          }        
          default:{      
            System.out.println("Dice 1: " + dice1);         
            System.out.println("Dice 2: " + dice2);        
            System.out.println("Dice Total: " + total);        
            System.out.println("Roll Type: Easy Ten");       
            break;     
          }        
        }     
      }     
      case 11:{   
        System.out.println("Dice 1: " + dice1);       
        System.out.println("Dice 2: " + dice2);     
        System.out.println("Dice Total: " + total);      
        System.out.println("Roll Type: Yo-leven");      
        break;      
      }    
      case 12:{   
        System.out.println("Dice 1: " + dice1);     
        System.out.println("Dice 2: " + dice2);     
        System.out.println("Dice Total: " + total);     
        System.out.println("Roll Type: Boxcars");     
        break;        
      }      
    }    
  }
}