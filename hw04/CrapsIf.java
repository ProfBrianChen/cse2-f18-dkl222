//David K. Lee
//9.24.2018
//CrapsIf.java
/////////////////
//importing Scanner method
import java.util.Scanner;

public class CrapsIf{
  public static void main (String args[]){
    //dices declared and initalized
    int randomDie1 = (int)(Math.random() * 5) + 1;
    int randomDie2 = (int)(Math.random() * 5) + 1;
    int randomScore = randomDie1 + randomDie2;
    
    Scanner chooseDie1 = new Scanner(System.in);
    Scanner chooseDie2 = new Scanner(System.in);
    
    Scanner randomChoose = new Scanner(System.in);
    
    System.out.println("Would you like to randomly cast the dice or choose two die values?");
    System.out.println("Enter \"0\" for RANDOM");
    System.out.println("Enter \"1\" for CHOOSE");
    int answer = randomChoose.nextInt();
    
    //if player wanted random dice
    if (answer = 0){
      //if the score totals 2
      if (randomScore = 2){
        System.out.println("First die: " + randomDie1);
        System.out.println("Second die: " + randomDie2);
        System.out.println("Total Roll: " + score);
        System.out.println("Roll Type: Snake Eyes");
      }
      //if score is 3
      else if (randomScore = 3){
        System.out.println("First die: " + randomDie1);
        System.out.println("Second die: " + randomDie2);
        System.out.println("Total Roll: " + score);
        System.out.println("Roll Type: Ace Deuce");
      }
      //if score is 4
      else if (randScore = 4){
        if (randomDie1 = 2){
          System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Hard Four");
        }
        else{
          System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Easy Four");
        }
      }
      //if score is 5
      else if (randomScore = 5){
          System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Fever Five");
        }
      //if score is 6
      else if (randomScore = 6){
        if (randomDie1 = 3){
          System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Hard Six");
        }  
        else{
          System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Easy Six");
        }
      }
      //if score is 7
      else if (randomScore = 7){
        System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Seven Out");
      }
      //if score is 8
      else if (randomScore = 8){
        if (randomDie1 = 4){
          System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Hard Eight");
        }
        else{
          System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Easy Eight");
        }
      }
      //if score is 9
      else if (randomScore = 9){
        System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Nine");
      }
      //if score is 10
      else if (randomScore = 10){
        if (randomDie1 = 5){
          System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Hard Ten");
        }
        else {
          System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Easy Ten");
        }
      }
    }
      //if score is 11
      else if (randomScore = 11){
        System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Yo-leven");
      }
      //if score is 12
      else{
        System.out.println("First die: " + randomDie1);
          System.out.println("Second die: " + randomDie2);
          System.out.println("Total Roll: " + score);
          System.out.println("Roll Type: Boxcars");
      }
    }
    
    //if player wanted to choose the dice
    if (answer = 1){
      System.out.println("Select the first dice number from 1 to 6: ");
      int die1 = chooseDie1.nextInt();
      System.out.println("Select the second dice number from 1 to 6: ");
      int die2 = chooseDie2.nextInt();
      int totalDie = die1 + die2;
      
      //if the score totals 2
      if (totalDie = 2){
        System.out.println("First die: " + die1);
        System.out.println("Second die: " + die2);
        System.out.println("Total Roll: " + totalDie);
        System.out.println("Roll Type: Snake Eyes");
      }
      //if score is 3
      else if (totalDie = 3){
        System.out.println("First die: " + die1);
        System.out.println("Second die: " + die2);
        System.out.println("Total Roll: " + totalDie);
        System.out.println("Roll Type: Ace Deuce");
      }
      //if score is 4
      else if (totalDie = 4){
        if (die1 = 2){
          System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Hard Four");
        }
        else{
          System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Easy Four");
        }
      }
      //if score is 5
      else if (totalDie = 5){
          System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Fever Five");
        }
      //if score is 6
      else if (totalDie = 6){
        if (die1 = 3){
          System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Hard Six");
        }  
        else{
          System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Easy Six");
        }
      }
      //if score is 7
      else if (totalDie = 7){
        System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Seven Out");
      }
      //if score is 8
      else if (totalDie = 8){
        if (die1 = 4){
          System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Hard Eight");
        }
        else{
          System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Easy Eight");
        }
      }
      //if score is 9
      else if (totalDie = 9){
        System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Nine");
      }
      //if score is 10
      else if (totalDie = 10){
        if (die1 = 5){
          System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Hard Ten");
        }
        else{
          System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Easy Ten");
        }
      }
      //if score is 11
      else if (totalDie = 11){
        System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Yo-leven");
      }
      //if score is 12
      else{
        System.out.println("First die: " + die1);
          System.out.println("Second die: " + die2);
          System.out.println("Total Roll: " + totalDie);
          System.out.println("Roll Type: Boxcars");
      }
    }
  }
}