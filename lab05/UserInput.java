import java.util.Scanner;

public class UserInput{
  
	public static void main(String[] args) {
		Scanner scnr = new Scanner(System.in);
    boolean correctCourse = false;
    boolean correctDepartment = false;
    boolean correctMeet = false;
    boolean correctTime = false;
    boolean correctInstructor = false;
    boolean correctStudent = false;
    
    int courseNum = 0;
    String departmentName = "";
    int meet = 0;
    int time = 0;
    String instructor = "";
    int student = 0;
    
    //Course Number
    System.out.println("Enter course number (integer): ");
    
		while (!correctCourse){
      correctCourse = scnr.hasNextInt();
      if(correctCourse == true){
        courseNum = scnr.nextInt();
      }
      else if(correctCourse == false){
        System.out.println("Incorrect Input. Input course number as integer:");
        scnr.next();
      }
		}
    
    //Department Name
    System.out.println("Enter the department name (String):");
    
    while (!correctDepartment){
      correctDepartment = scnr.hasNext();
      if (correctDepartment == true){
        departmentName = scnr.next();
      }
      else if (correctDepartment == false){
        System.out.println("Incorrect input. Input department name as string:");
        scnr.next();
      }
    }
    
    //Times course meet in week
    System.out.println("Enter the # of times the course meets in a week (integer):");
    
    while (!correctMeet){
      correctMeet = scnr.hasNextInt();
      if (correctMeet == true){
        meet = scnr.nextInt();
      }
      else if (correctMeet == false){
        System.out.println("Incorrect Input. Input # of meeting times as integer:");
        scnr.next();
      }
    }
    
    //Time of day course begins
    System.out.println("Enter the time of day the course begins (integer/ 24-hr format):");
    
    while (!correctTime){
      correctTime = scnr.hasNextInt();
      if (correctTime == true){
        time = scnr.nextInt();
      }
      else if (correctTime == false){
        System.out.println("Incorrect input. Input the time of day the course begins as integer in 24-hr format:");
        scnr.next();
      }
    }
    
    //Instructor Name
    System.out.println("Enter your instructor name:");
    
    while (!correctInstructor){
      correctInstructor = scnr.hasNext();
      if (correctInstructor == true){
        instructor = scnr.next();
      }
      else if (correctInstructor == false){
        System.out.println("Incorrect input. Input your instructor name as String:");
        scnr.next();
      }
    }
    
    //Num of student in class
    System.out.println("Enter the # of students in your class:");
    
    while (!correctStudent){
      correctStudent = scnr.hasNextInt();
      if (correctStudent == true){
        student = scnr.nextInt();
      }
      else if (correctStudent == false){
        System.out.println("Incorrect input. Input the # of students in your class:");
        scnr.next();
      }
    }
    
    System.out.println("Course Number: " + courseNum);
    System.out.println("Department Name: " + departmentName);
    System.out.println("Meeting times a week: " + meet);
    System.out.println("Starting time (24-hr format): " + time);
    System.out.println("Instructor's Name: " + instructor);
    System.out.println("Student Headcount: " + student);
    
	}
}