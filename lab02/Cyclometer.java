//David K. Lee
//9.6.2018
//CSE 002 Cyclometer
///////////////////////
public class Cyclometer{
  //Main method for the start of the program
  public static void main (String []args){
    
    //Variables
    double secsTrip1 = 480.0; //Seconds elapsed in 1st trip
    double secsTrip2 = 3220.0; //Seconds elapsed in 2nd trip
    int countsTrip1 = 1561; //Cycle counts in 1st trip
    int countsTrip2 = 9037; //Cycle counts in 2nd trip
    
    //Constant variables
    double wheelDiameter = 27.0, //Diameter of the wheel
    PI = 3.14159, //Value of Pi
    feetPerMile = 5280.0, //Mile to feet conversion
    inchesPerFoot = 12.0, //Foot to inch conversion
    secondsPerMinute = 60.0; //Minute to second conversion
    double distanceTrip1, distanceTrip2, totalDistance; //Uninitialized doubles
    
    //Prints for time of trips in minutes
    System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) +
                       " minutes and had " + countsTrip1 + " counts.");
    System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) +
                       " minutes and had " + countsTrip2 + " counts.");
    
    //Distance calculations
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; //1st trip distance in inches
    distanceTrip1 /= inchesPerFoot * feetPerMile; //Converting Trip1, inches to miles
    distanceTrip2 = countsTrip2 * wheelDiameter * PI; //2nd trip distance in inches
    distanceTrip2 /= inchesPerFoot * feetPerMile; //Converting Trip2, inches to miles
    totalDistance = distanceTrip1 + distanceTrip2; //Total distance from calculated distance of trips
    
    //Printing output
    System.out.println("Trip 1 was " + distanceTrip1 + " miles.");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles.");
    System.out.println("The total distance was " + totalDistance + " miles.");
    
    
  } //Ending the main method
} //Ending the class