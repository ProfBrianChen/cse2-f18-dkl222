//David K. Lee
//9.15.2018
//Pyramid.java
////////////////

//importing Scanner method
import java.util.Scanner;

public class Pyramid{
  public static void main(String args[]){
    //variable declaration
    Scanner myScanner = new Scanner(System.in);
    int pyramidSquareSide;
    int pyramidHeight;
    double pyramidVolume;
    
    //variable inputs
    System.out.println("The square side of the pyramid is (input length): ");
    pyramidSquareSide = myScanner.nextInt();
    System.out.println("The height of the pyramid is (input height): ");
    pyramidHeight = myScanner.nextInt();
    
    //calculations
    pyramidVolume = (pyramidSquareSide * pyramidSquareSide * pyramidHeight) / 3;
    
    System.out.println("The volume inside the pyramid is: " + pyramidVolume);
  }
}