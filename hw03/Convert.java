//David K. Lee
//9.15.2018
//Convert.java
///////////////////

//importing Scanner method
import java.util.Scanner;

public class Convert{
  public static void main(String args[]){
    
    //declaring variables
    Scanner myScanner = new Scanner(System.in);
    double affectedAcres;
    int rainInches;
    double acreToMile;
    double inchToMile;
    double totalVolume;
    
    //Variable Input
    System.out.println("Enter the affected area in acres (in xx.xx format): ");
    affectedAcres = myScanner.nextDouble();
    System.out.println("Enter the inches of rainfall in the affected area: ");
    rainInches = myScanner.nextInt();
    
    //Conversion and sum
    acreToMile = affectedAcres * 0.0015625;
    inchToMile = (rainInches * 0.0000157828);
    totalVolume = acreToMile * inchToMile;
    
    //final output
    System.out.println(totalVolume + " cubic miles");
  }
} 