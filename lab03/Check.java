//David K. Lee
//9.13.2018
//Check Lab03
///////////////
//importing Scanner method to use
import java.util.Scanner;

public class Check{
  
  public static void main (String args[]){
    //inputting new Scanner method
    Scanner myScanner = new Scanner(System.in);
    
    //cost input
    System.out.println("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();
    
    //tip percentage input
    System.out.println("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;         
    
    //enter # of people
    System.out.println("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    //Input Summary
    System.out.println("");
    System.out.println("Original Cost: $" + checkCost);
    System.out.println("Tip Percentage: " + tipPercent + "%");
    System.out.println("Number of People: " + numPeople);
    
    //Declaring variables
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;
    
    //Individual cost calculations
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int)costPerPerson;
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
  }
}