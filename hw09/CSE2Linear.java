import java.util.Scanner;

public class CSE2Linear {

	public static void main(String[] args) {
		Scanner scnr = new Scanner (System.in);
		final int ARRAY_SIZE = 15;
		int[] finalGrades = new int[ARRAY_SIZE];
		
		//Input grades into array with restrictions
		for(int i = 0; i < ARRAY_SIZE; i++) {
			System.out.print("Please insert integer between 0 and 100: ");
			int testInput = scnr.nextInt();
			
			if (testInput < 0 && testInput > 100) {
				while (testInput < 0 && testInput > 100) {	
					System.out.println("Not an integer between 0 and 100.");
					System.out.print("Please insert integer between 0 and 100: ");
					testInput = scnr.nextInt();
				}
			}
			
			if (i > 0) {
				while (testInput < finalGrades[i - 1]) {
					System.out.println("Not greater than the previous input.");
					System.out.print("Please insert integer between 0 and 100 greater than the previous input: ");
					testInput = scnr.nextInt();
				}
				
			}
			finalGrades[i] = testInput;
		}
		
		//Print out array
		System.out.println("Grades given:");
		for (int j = 0; j < ARRAY_SIZE; j++) {
			System.out.print(finalGrades[j] + " ");
		}
		System.out.println("");
		
		//Binary Search
		System.out.print("Enter grade to search for: ");
		int gradeSearch = scnr.nextInt();
		
		int searchResult = gradeSearch (finalGrades, ARRAY_SIZE, gradeSearch);
		
		if (searchResult == -1) {
			System.out.println(gradeSearch + " was not found in the list.");
		}
		else {
			System.out.println("Found " + gradeSearch);
		}
	}
	
	//Binary search field
	public static int gradeSearch (int[] gradeArray, int arraySize, int searchNum) {
		int middle;
		int up = arraySize - 1;
		int down = 0;
		
		//The actual search
		while (up >= down) {
			middle = (up + down) / 2;
			if (gradeArray[middle] < searchNum) {
				down = middle + 1;
			}
			else if (gradeArray[middle] > searchNum) {
				up = middle - 1;
			}
			else {
				return middle;
			}
		}
		//When the grade can't be found
		return -1;
	}

}