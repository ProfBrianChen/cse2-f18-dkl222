//David K. Lee
//9.20.2018
//CardGenerator.java
/////////////////////

public class CardGenerator{
  public static void main (String args[]){
    
    //Declaring variables
    //Random method
    int magicDeck = (int)(Math.random() * 51) + 1;
    String cardType;
    String cardName = "";
    boolean name = true;
    
    //Assigning numbers by card type and number
    //If random generator is less than/ equal to 13
    if (magicDeck <= 13){
      cardType = "Diamonds";
      
      if (magicDeck == 1){
        cardName = "Ace";
        name = false;
      }
      else if (magicDeck == 11){
        cardName = "Jack";
        name = false;
      }
      else if (magicDeck == 12){
        cardName = "Queen";
        name = false;
      }
      else if (magicDeck == 13){
        cardName = "King";
        name = false;
      }
    }
    //If generator less than/ equal to 26
    else if (magicDeck <= 26){
      cardType = "Clubs";
      magicDeck -= 13;
      
      if (magicDeck == 1){
        cardName = "Ace";
        name = false;
      }
      else if (magicDeck == 11){
        cardName = "Jack";
        name = false;
      }
      else if (magicDeck == 12){
        cardName = "Queen";
        name = false;
      }
      else if (magicDeck == 13){
        cardName = "King";
        name = false;
      }
    }
    //If generator less than/ equal to 39
    else if (magicDeck <= 39){
      cardType = "Hearts";
      magicDeck -= 26;
      
      if (magicDeck == 1){
        cardName = "Ace";
        name = false;
      }
      else if (magicDeck == 11){
        cardName = "Jack";
        name = false;
      }
      else if (magicDeck == 12){
        cardName = "Queen";
        name = false;
      }
      else if (magicDeck == 13){
        cardName = "King";
        name = false;
      }
    }
    //If generator less than/ equal to 52
    else{
      cardType = "Spades";
      magicDeck -= 39;
      
      if (magicDeck == 1){
        cardName = "Ace";
        name = false;
      }
      else if (magicDeck == 11){
        cardName = "Jack";
        name = false;
      }
      else if (magicDeck == 12){
        cardName = "Queen";
        name = false;
      }
      else if (magicDeck == 13){
        cardName = "King";
        name = false;
      }
    }
    
    if (name = true){
      System.out.println("You picked the " + magicDeck + " of " + cardType);
    }
    else if (name = false){
     System.out.println("You picked the " + cardName + " of " + cardType); 
    }
  }
}