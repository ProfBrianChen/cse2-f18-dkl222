//David K. Lee
//9.9.2018
////////////////
public class Arithmetic{
  
  public static void main (String args[]){
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost perbelt
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    //total cost of each article type
    double totalPantsCost = numPants * pantsPrice;
    double totalShirtCost = numShirts * shirtPrice;
    double totalBeltCost = numBelts * beltCost;
    
    //sales tax cost on each item
    double pantsTax = (int)((totalPantsCost * paSalesTax) * 100) / 100.0;
    double shirtTax = (int)((totalShirtCost * paSalesTax) * 100) / 100.0;
    double beltTax = (int)((totalBeltCost * paSalesTax) * 100) / 100.0;
    
    //total cost of everything before tax
    double totalCost = totalPantsCost + totalShirtCost + totalBeltCost;
    
    //total sales tax, w/out cost
    double totalTax = pantsTax + shirtTax + beltTax;
    
    //Total cost w/ sales tax
    double total = totalCost + totalTax;
    
    //Text Output
    System.out.println("");
    System.out.println("ARIELLE CARR'S \"CSE MART\"");
    System.out.println("");
    System.out.println("DATE: 09/11/2018");
    System.out.println("TIME: 23:00.00 (exactly)");
    System.out.println("You were served by: DAVID");
    System.out.println("");
    System.out.println("DESCRIPTION");
    System.out.println("-------------------------------");
    System.out.println("");
    System.out.println("--Cost of each article type-------");
    System.out.println("Total cost of pants:\t$" + totalPantsCost);
    System.out.println("Total cost of shirts:\t$" + totalShirtCost);
    System.out.println("Total cost of belts:\t$" + totalBeltCost);
    System.out.println("");
    System.out.println("--Sales Tax of each article type--");
    System.out.println("Sales tax for pants:\t$" + pantsTax);
    System.out.println("Sales tax for shirts:\t$" + shirtTax);
    System.out.println("Sales tax for belts:\t$" + beltTax);
    System.out.println("");
    System.out.println("--Totals--------------------------");
    System.out.println("Total without tax:\t$" + totalCost);
    System.out.println("Total sales tax:\t$" + totalTax);
    System.out.println("Grand Total w/ tax:\t$" + total);
    System.out.println("");
  }
}