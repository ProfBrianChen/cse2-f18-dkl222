//David K. Lee
//9.3.2018
/////////////////
//CSE 002 Welcome Class

public class WelcomeClass{
  
  public static void main(String args[]){
    //Welcoming section
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    
    //ID tag
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-D--K--L--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    
    //Tweet-sized Autobiography
    System.out.println("I am an American-native Korean-American.");
    System.out.println("I lived in New Jersey until I moved to");
    System.out.println("Afghanistan in 2012. Moving around the");
    System.out.println("world, I lived in many places and picked");
    System.out.println("up many interests.");
    System.out.println("I am a jack-of-all-trades...");
    System.out.println("...meaning I know nothing at all...");
  }
}