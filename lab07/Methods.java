public class Methods {

	//adjective generator method
	public static String adjectiveInput () {
		int randomNum = (int)(Math.random() * 8) + 1;
		String adjOutput = "";
		
		switch (randomNum) {
		case 1: adjOutput = "stationary";
		break;
		case 2: adjOutput = "thin";
		break;
		case 3: adjOutput = "fast";
		break;
		case 4: adjOutput = "complex";
		break;
		case 5: adjOutput = "cunning";
		break;
		case 6: adjOutput = "surreal";
		break;
		case 7: adjOutput = "annoying";
		break;
		case 8: adjOutput = "tiring";
		break;
		case 9: adjOutput = "primitive";
		break;
		}
		
		return adjOutput;
	}
	
	//Non-primary subject noun generator method
	public static String subjectNounInput () {
		int randomNum = (int)(Math.random() * 8) + 1;
		String sNounOutput = "";
		
		switch (randomNum) {
		case 1: sNounOutput = "foxes";
		break;
		case 2: sNounOutput = "chairs";
		break;
		case 3: sNounOutput = "pillows";
		break;
		case 4: sNounOutput = "computers";
		break;
		case 5: sNounOutput = "rickshaws";
		break;
		case 6: sNounOutput = "dangos";
		break;
		case 7: sNounOutput = "noodles";
		break;
		case 8: sNounOutput = "wasabi";
		break;
		case 9: sNounOutput = "zombies";
		break;
		}
		
		return sNounOutput;
	}
	
	//Non-primary object noun generator method
	public static String objectNounInput () {
		int randomNum = (int)(Math.random() * 8) + 1;
		String oNounOutput = "";
		
		switch (randomNum) {
		case 1: oNounOutput = "guns";
		break;
		case 2: oNounOutput = "stack of papers";
		break;
		case 3: oNounOutput = "pillars";
		break;
		case 4: oNounOutput = "equestrians";
		break;
		case 5: oNounOutput = "pencils";
		break;
		case 6: oNounOutput = "hearthstones";
		break;
		case 7: oNounOutput = "extinguishers";
		break;
		case 8: oNounOutput = "wigs";
		break;
		case 9: oNounOutput = "dragons";
		break;
		}
		
		return oNounOutput;
	}
	
	//Past-tense verbs generator method
	public static String verbInput () {
		int randomNum = (int)(Math.random() * 8) + 1;
		String verbOutput = "";
		
		switch (randomNum) {
		case 1: verbOutput = "ate";
		break;
		case 2: verbOutput = "stole";
		break;
		case 3: verbOutput = "jabbed";
		break;
		case 4: verbOutput = "blocked";
		break;
		case 5: verbOutput = "senesed";
		break;
		case 6: verbOutput = "craved";
		break;
		case 7: verbOutput = "extinguished";
		break;
		case 8: verbOutput = "stalked";
		break;
		case 9: verbOutput = "drove";
		break;
		}
		
		return verbOutput;
	}
	
	//Present tense verbs generator method
	public static String pVerbInput() {
		int randomNum = (int)(Math.random() * 8) + 1;
		String pVerbOutput = "";
		
		switch (randomNum) {
		case 1: pVerbOutput = "lick";
		break;
		case 2: pVerbOutput = "fight";
		break;
		case 3: pVerbOutput = "strike";
		break;
		case 4: pVerbOutput = "bump";
		break;
		case 5: pVerbOutput = "slap";
		break;
		case 6: pVerbOutput = "turn";
		break;
		case 7: pVerbOutput = "run";
		break;
		case 8: pVerbOutput = "fly";
		break;
		case 9: pVerbOutput = "drop";
		break;
		}
		
		return pVerbOutput;
	}
	
	
	
	//main method
	public static void main(String[] args) {
	
		//adjective 1
		String adj = adjectiveInput();
		//adjective 2
		String adj2 = adjectiveInput();
		while (adj2 == adj) {
			adj2 = adjectiveInput();
		}
		//adjective 3
		String adj3 = adjectiveInput();
		while (adj3 == adj2) {
			while (adj3 == adj) {
				adj3 = adjectiveInput();
			}
			adj3 = adjectiveInput();
		}
		//adjective 4
		String adj4 = adjectiveInput();
		while (adj4 == adj3) {
			while (adj4 == adj2) {
				while (adj4 == adj) {
					adj4 = adjectiveInput();
				}
				adj4 = adjectiveInput();
			}
			adj4 = adjectiveInput();
		}
		

		//Object Noun 1
		String oNoun = objectNounInput();
		//Object Noun 2
		String oNoun2 = objectNounInput();
		while (oNoun2 == oNoun) {
			oNoun2 = objectNounInput();
		}
		//Object Noun 3
		String oNoun3 = objectNounInput();
		while (oNoun3 == oNoun2) {
			while (oNoun3 == oNoun) {
				oNoun3 = objectNounInput();
			}
			oNoun3 = objectNounInput();
		}
		//Object Noun 4
		String oNoun4 = objectNounInput();
		while (oNoun4 == oNoun3) {
			while (oNoun4 == oNoun2) {
				while (oNoun4 == oNoun) {
					oNoun4 = objectNounInput();
				}
			oNoun4 = objectNounInput();
			}
		oNoun4 = objectNounInput();
		}
		
		String sNoun = subjectNounInput();
		String sNoun2 = sNoun;
		String verb = verbInput();
		String pVerb = pVerbInput();
		
		System.out.println("The " + adj + ", " + adj2 + " " + sNoun + " " + verb + " the " + adj3 + " " + oNoun + ".");

		int sen2gen = (int)(Math.random() + 1);
		
		if (sen2gen == 1) {
			System.out.println("The " + sNoun2 + " used " + oNoun2 + " to " + pVerb + " " + oNoun3 + " at the " + adj + " " + oNoun4 + ".");
		}
		else {
			System.out.println("It " + " used " + oNoun2 + " to " + pVerb + " " + oNoun3 + " at the " + adj + " " + oNoun4 + ".");
		}
	}

}