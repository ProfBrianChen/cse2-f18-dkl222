import java.util.Scanner;
public class Shuffling {

	public static void main(String[] args) {
		Scanner scnr = new Scanner(System.in);
		
		System.out.print("Input number of hands to pull out: ");
		int chosenCards = scnr.nextInt();
		
		//Card suits
		String[] suitName = {"C", "H", "S", "D"};
		String[] rankName = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
		String[] cards = new String[52];
		String[] hands = new String[5];
		
		//Declaring and initializing
		int numCards = 0;
		int again = 1;
		int index = 51;
		
		//Initializing String[] cards' faces
		for (int i = 0; i < cards.length; i++) {
			cards[i] = rankName [i % 13] + suitName [i / 13];
			printArray(cards, i);
		}
		System.out.println("");
		
		shuffle(cards);
		
		getHand(cards, chosenCards);
		
	}
	
	//To print out array
	public static void printArray(String[] list, int num) {
		System.out.print(list[num] + " ");
	}
	
	//To shuffle the array
	public static void shuffle(String[] list) {
		String holder;
		
		for (int i = 0; i < list.length; i++) {
			int randomNum = (int)Math.random();
			if (randomNum == 0){
				holder = list[0];
				list[0] = list[i];
				list[i] = holder;
				System.out.print(list[i] + " ");
			}
			else {
				System.out.print(list[i] + " ");
			}
		}
		System.out.println("");
	}
	
	//To pull numbers of cards
	public static void getHand(String[] list, int num) {
		for (int i = list.length - 1; i >= list.length - num; i--) {
			System.out.print(list[i] + " ");
		}
	}
	
}