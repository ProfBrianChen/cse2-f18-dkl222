public class Array {
	public static void main(String[] args) {
		
		//initializing the arrays
		int[] array1 = new int[100];
		int[] array2 = new int[100];
		
		for (int i = 0; i < array1.length; i++){
			array1[i] = (int)(Math.random() * 99);
		}
		
		
		for (int j = 0; j < array2.length; j++){
			int count = 0;

			for (int k = 0; k < array1.length; k++){
					if (array2[j] == array1[k]){
						count += 1;
					}
			}
			System.out.println(j + " occurs " + count + " times.");
		}
	}
}